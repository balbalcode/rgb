const state = () => ({
    PRODUCTS: [],
    PRODUCT: []
})

const mutations = {
    SET_PRODUCTS(state, data) {
        state.PRODUCTS = data
    },
    SET_PRODUCT(state, data) {
        state.PRODUCT = data
    }
}

const getters = {
    getProductBySlug: (state) => (slug) => {
        return state.PRODUCTS.find(opt => opt.attributes.slug === slug)
    },
}

const actions = {
    async get_product({commit}, payload) {
        try {
            const res = await this.$repositories.product.all(payload.pagination, payload.filter, payload.order)
            let {data} = res.data
            if (res.status === 200 && data) {
                commit('SET_PRODUCTS', data)
            }
        } catch (e) {
            this.$sentry.captureException(e)
            throw e
        }
    },

    async get_product_detail({commit}, payload) {
        try {
            const res = await this.$repositories.product.show(payload.id)
            let {data} = res.data
            if (res.status === 200 && data) {
                commit('SET_PRODUCT', data)
                return data
            }
        } catch (e) {
            this.$sentry.captureException(e)
            throw e
        }
    },

}
export default {
    state,
    getters,
    actions,
    mutations,
};