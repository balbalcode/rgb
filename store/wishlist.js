const state = () => ({

})

const mutations = {}

const getters = {}

const actions = {
    async create({commit}, payload) {
        try {
            const res = await this.$repositories.wishlist.create(payload)
            let {data} = res.data
            if (res.status === 200 && data) {
                return data
            }
        } catch (e) {
            this.$sentry.captureException(e)
            throw e
        }

    },

}
export default {
    state,
    getters,
    actions,
    mutations,
};