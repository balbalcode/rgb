# RGB Recruitment by balbal hosted at [rgb.balbal.my.id](https://rgb.balbal.my.id)

[![N|Solid](https://assets.balbal.my.id/rgb/logo.png)]()

## Tech stack

* NUXT JS -> next VueJs application
* Vue JS -> progressive javascript framework for frontend
* Bootstrap Vue -> layout and theming
* Axios -> http request

#### Detail stack

I am using REPOSITORY PATTERN for API service and vuex management, more information please read at
[NUXT REPOSITORY PATTERN](https://github.com/Kaperskyguru/nuxt-with-repository-pattern)

This project consuming ATOMIC DESIGN pattern for each component. this is can help us to focus while maintaining the
component because each component will isolated and defined to small parts. More information about atomic design please
read [Atomic design Pattern](https://atomicdesign.bradfrost.com)

The product image was added lazyload, loading indicator and error image default. Iam
using [vue-lazyload](https://www.npmjs.com/package/vue-lazyload) for make sure image component can render properly.

## Why iam using NUXT?

As a common knowledge nuxt is an open source source framework making web development simple and powerful with vue js. i
want to take a simple way for configuring the directory so that i am using NUXT. don't worry, nuxt are built with vue.

Work in progress

- [x] filter product
- [x] sort product
- [x] filter and sort product at sametime
- [x] combination of filter product
- [x] responsive mobile screen
- [x] wishlist (idk what happens with our API, after add new wishlist api gift our 200 response but the data null, but when i hit product list again, the product still not mark as wishlist product)
- [x] base meta description for sharing link
- [x] open graph meta (og:meta) [need to test]
- [x] seo score attached at this file, iam using seobility.net for check
- [ ] nuxt-pwa
- [ ] detail product

## Installation

1. setup .env file
   ```shell
   API_URL=
   SENTRY_DSN=
    ```
   or you can see the env.example (i was config for sentry also firebase. but i delete it)
2. Run script npm install
   ```shell
   npm install
    ```
3. Then run project based on build case

   #### Compiles and hot-reloads for development
    ```shell
   npm run dev
    ```
   it's will be start the development mode this project, default port is 8000. so you can access it
   on http://localhost:8000/

   #### Compiles and minifies for production
    ```shell
   npm run build
    ```
   it's will generate a file to serve if you want to run ssr mode by running `npm run start`

   #### generate static file to serve (SSG)
   ```shell
   npm run generate
   ```

   it's will be generate static file in directory /dist. hit the index.html for running the project
   
    
## Others information
##### web preview
[![N|Solid](https://assets.balbal.my.id/rgb/preview.gif)]()
##### seo score

[![N|Solid](https://assets.balbal.my.id/rgb/seo.png)]()