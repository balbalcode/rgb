function navigation(index) {
    let list = [
        {value: "RATE", text: "Rating 4 Keatas"},
        {value: "STOCK", text: "Stock Tersedia"},
    ]
    return (isFinite(index)) ? list[index] : list
}

function filter(index) {
    let list = [
        {value: "NEWEST", text: "Terbaru"},
        {value: "HIGHEST", text: "Ulasan Tertinggi"},
        {value: "LOWEST", text: "Ulasan Terendah"},
    ]
    return (isFinite(index)) ? list[index] : list
}


export default {
    navigation, filter
}