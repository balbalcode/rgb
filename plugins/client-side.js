import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import NuxtSSRScreenSize from 'nuxt-ssr-screen-size'
import noImage from './../static/images/no-image.png'
import loadingImage from './../static/images/loader.gif'

Vue.use(NuxtSSRScreenSize)
Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: noImage,
    loading: loadingImage,
    attempt: 1
})