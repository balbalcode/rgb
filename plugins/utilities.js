import jscookie from "js-cookie"

export default ({
    removeSymbols(text) {
        return text.replace(/[^\w ]/g, '')
    },
    removeNumber(text) {
        return text.replace(/[^a-zA-z ]/g, "")
    },
    removeLetter(text) {
        return text.replace(/[^0-9 ]/g, "")
    },
    getAssetUrl() {
        return process.env.ASSET_URL
    },
    getPrimaryColor() {
        return process.env.PRIMARY_COLOR
    },
    convertToRupiah(number) {
        number = number.toString()
        number = number.replace(/[^0-9 ]/g, "")
        var rupiah = '';
        var number_rev = number.toString().split('').reverse().join('');
        for (var i = 0; i < number_rev.length; i++) if (i % 3 == 0) rupiah += number_rev.substr(i, 3) + '.';
        rupiah = rupiah.split('', rupiah.length - 1).reverse().join('');
        return (rupiah.length < 1 ? '0' : rupiah);
    },
    convertToNumber(rupiah) {
        if (rupiah.length > 1) {
            return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);
        } else {
            return rupiah
        }
    },
    getCookie(name) {
        return jscookie.get(name)
    },

    setCookie(name, value, expired = 7) {
        jscookie.set(name, value, {expires: expired})
    }

})