export default function ({app}) {
    app.$axios.defaults.baseURL = process.env.API_URL
    app.$axios.setHeader('accept-encoding', null)
    app.$axios.setHeader('Content-Type', 'application/json')
    app.$axios.onError(async error => {
        app.$sentry.captureMessage(error)
        return Promise.reject(error);
    })
}