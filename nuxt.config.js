import router from "./config/router";


module.exports = {
    ssr: true,
    target: 'static',
    server: {
        port: 3883,
        host: 'localhost',
        timing: false
    },

    head: {
        title: 'RGB Recruitment ',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {
                hid: 'description',
                name: 'description',
                content: "RGB Recruitment for job application at november 2021"
            },
            {property: "og:title", content: "RGB Recruitment"},
            {property: "fb:app_id", content: "263935132355700"},
            {property: "og:image", content: "https://assets.balbal.my.id/rgb/logo-square.png"},
            {property: "og:url", content: "https://rgb.balbal.my.id"},
            {
                property: "og:description",
                content: "RGB Recruitment for job application at november 2021"
            },
            {property: "og:image:width", content: "256"},
            {property: "og:image:height", content: "256"},
            {property: "og:image:type", content: "image/png"},
        ],
        link: [
            {
                rel: 'stylesheet',
                href:
                    'https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700;900&display=swap'
            },

        ],
    },

    loading: {color: process.env.PRIMARY_COLOR || '#FFC348'},

    css: [
        '@/assets/index.scss',
        'vue-multiselect/dist/vue-multiselect.min.css'
    ],

    plugins: [
        '~/plugins/injector',
        '~/plugins/vuelidate',
        '~/plugins/repository',
        {ssr: false, src: '~plugins/client-side.js'},
    ],

    axios: {
        baseURL: process.env.API_URL || 'https://jsonplaceholder.typicode.com'
    },

    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/sentry',
        ['@nuxtjs/bootstrap-vue', {css: false}]
    ],

    sentry: {
        dsn: process.env.SENTRY_DSN || '',
        lazy: true
    },

    router: {
        extendRoutes(routes) {
            router.forEach(async (item) => {
                routes.push(item)
            })
            return routes
        }
    },
    build: {
        extend(config, ctx) {
            if (ctx.isDev) {
                config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
            }
        }
    },
    env: {
        API_URL: process.env.API_URL || '',
        ASSET_URL: process.env.ASSET_URL || '',
        PRIMARY_COLOR: process.env.PRIMARY_COLOR || ''
    }
}
