import ProductRepository from '~/repositories/ProductRepository'
import WishlistRepository from '~/repositories/WishlistRepository'

export default ($axios) => ({
  product: ProductRepository($axios),
  wishlist: WishlistRepository($axios),
})