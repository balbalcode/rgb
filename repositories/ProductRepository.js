import {query_helper} from "~/repositories/config";

const resource = '/gifts'
export default ($axios) => ({
    all(pagination, filter, order) {
        return $axios.get(`${resource}?${query_helper(pagination, filter, order)}`)
    },

    show(id) {
        return $axios.get(`${resource}/${id}`)
    },

})