export function query_helper(pagination, filter, order) {

    // its for destructing pagination object
    let paging = (Object.entries(pagination).length > 0) ? `page[number]=${pagination.currentPage}&page[size]=${pagination.itemPerPage}` : ''

    // its for destructing filter object
    // so the json will be translated to filter like this &JSONKEY=JSONVALUE
    let filtering = ''
    if (filter.length > 0) {
        filter.forEach((item) => {
            filtering += `&filter[${item.key}]=${item.value}`
        });
    }


    let ordering = (Object.entries(order).length > 0) ? `&order[${order.type}]=${order.query}` : ''
    return `${paging}${filtering}${ordering}`
}