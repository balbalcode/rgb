const resource = '/gifts'
export default ($axios) => ({
  create(payload) {
    return $axios.post(`${resource}/${payload.id}/wishlist`, payload)
  },
})