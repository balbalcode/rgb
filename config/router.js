const router = [
    {
        path: "/",
        component: "~/pages/main/index.vue",
        chunkNames: "/"
    }
]
export default router;
